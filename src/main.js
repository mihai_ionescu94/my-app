import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import Routes from './routes';
import './firebase';
import VueFire from 'vuefire';

Vue.use(VueFire);
Vue.use(VueResource);
Vue.use(VueRouter);

const router = new VueRouter({
  routes: Routes
  
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router: router 
}).$mount('#app');

