import { initializeApp } from 'firebase'

const app = initializeApp ({
    apiKey: "AIzaSyAcCjysAIiKrf6h-Tbohlrnniq2PzUhvhI",
    authDomain: "tema-irinel.firebaseapp.com",
    databaseURL: "https://tema-irinel.firebaseio.com",
    projectId: "tema-irinel",
    storageBucket: "tema-irinel.appspot.com",
    messagingSenderId: "636927983441"
  });

  export const db = app.database();
  export const namesRef = db.ref('names');